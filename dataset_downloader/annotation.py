"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
annotation.py:
    Creates train.txt with coco data
"""

import json
import cv2

def annotate():
    """
    Open and read dataset file in .json format containing annotations creating a "train.txt"

    Args:
        None
    
    Returns:
        None
    """
    with open('ann.json') as json_file:
        ann_dict = json.load(json_file)

    fp = open("dataset/train.txt","w")

    for k in ann_dict.keys():
        a = ann_dict[k]
        img_file = k + ".jpg"
        fp.write("dataset/images/"+img_file + " ")
        for l in a:
            x1,y1,x2,y2,id = l[0],l[1],l[2],l[3],l[4]
            fp.write(str(x1) + "," + str(y1) + "," + str(x2) + "," + str(y2) + "," +str(id) + " ")
        fp.write("\n")



    
