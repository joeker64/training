"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
merge_coco_annotations.py:
    Organise all downloaded files from the COCO dataset
"""

import glob
import json
import cv2
from multiprocessing.pool import ThreadPool


class Merge_COCO_annotations:
    """
    Merge downloaded data with downloaded images
    """
    def __init__(self):
        """
        Create a "Merge_COCO_annotations" object

        Args:
            None

        Returns:
            Merge_COCO_annotations
        """
        with open('dataset_downloader/classes.txt', 'r') as fp:
            self.lines = fp.readlines()
        self.class_id = 0
        self.per_class_cnt = [0, 0, 0, 0, 0]
        self.MAX_THREADS = 24
        
    def get_annotations(self, file):
        """
        Read annotations data & images processing the data and collecting ground truths

        Args:
            file (string): Location of annotation file

        Returns:
            None
        """
        with open(file, 'r', encoding='utf-8') as read:
            ann = read.read()
    
        file_name = file.split("/")
        file_name = file_name[-1].split(".txt")[0]
        
        img_file = "downloaded_images/"+ self.class_name + "/" + file_name + ".jpg"        
        try: 
            img = cv2.imread(img_file)
            cv2.imwrite("dataset/images/" + file_name + ".jpg", img)
            print(f"Saved {img_file}")
        except:
            print("Print image",img_file)
            return
        
        ann = ann.split('\n')
        for an in ann:
            temp = []
            an = an.split()
            xmin = int(an[1])
            ymin = int(an[2])
            xmax = int(an[3])
            ymax = int(an[4])
            temp.append(xmin)
            temp.append(ymin)
            temp.append(xmax)
            temp.append(ymax)
            temp.append(self.class_id)
                
            if file_name not in self.ann_dict.keys():
                self.ann_dict[file_name] = []
                self.ann_dict[file_name].append(temp)
            else:
                self.ann_dict[file_name].append(temp)
                    
        self.per_class_cnt[self.class_id] += 1


    def merge(self):
        """
        Merge all the coco data together with images & meta-data creating "ann.json"

        Args:
            None

        Returns:
            None
        """
        for x in self.lines:
            self.class_name = x.split('\n')[0]
            if self.class_id == 0 :
                self.ann_dict = {}
            else:
                with open('ann.json') as json_file:
                    self.ann_dict = json.load(json_file)

            self.files = glob.glob("downloaded_images/"+ self.class_name + "/*.txt")

            self.curr_class = self.class_name
            with ThreadPool(self.MAX_THREADS) as p:
                p.map(self.get_annotations, self.files)

            with open("ann.json", "w") as outfile:
                json.dump(self.ann_dict, outfile)

            self.class_id = self.class_id + 1
            print("Total images downloaded", sum(self.per_class_cnt))
            print("Per class images", self.per_class_cnt)

#merger = Merge_COCO_annotations()
#merger.merge()

