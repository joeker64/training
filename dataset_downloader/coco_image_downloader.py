"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
coco_image_downloader.py:
    Download and process the COCO dataset used in training the ml models
"""

from pycocotools.coco import COCO
from multiprocessing.pool import ThreadPool
import requests
import os
import sys
import threading
import cv2
import asyncio


class Coco_Dataset_Downloader:
    """
    Download coco dataset
    """
    def __init__(self, classes, download_directory="downloaded_images"):
        """
        Create "Coco_Dataset_Downloader" object

        Args:
            classes (string array): List of classes that will be trained against 
            download_directory (string): Location of downloaded images

        Returns:
            Coco_Dataset_Downloader (object)
        """
        self.classes = classes
        self.download_directory = download_directory
        self.MAX_THREADS = 24
        self.curr_class = None
        self.except_images = open("excluded_images.txt", "w")
        self.img_cnt = 0

    def start(self):
        """
        Start downloading coco dataset

        Args:
            None

        Returns:
            None
        """
        self.coco = COCO('dataset_downloader/instances_train2017.json')
        self.preprocess_classes()
        urls, file_names = self.get_urls()
        self.download_using_thrds(urls)
        self.write_annotations()
    
    def makeDirectory(self, dirName):
        """
        Make directory for artifacts

        Args:
            dirName (string): Name of directory to create

        Returns:
            None
        """
        try:
            os.mkdir(dirName)
            print(f"Made {dirName} Directory.")
        except:
            print("Directory already created...")
    
    def preprocess_classes(self):
        """
        Format class data

        Args:
            None

        Returns:
            None
        """
        self.classes = [class_name.lower() for class_name in self.classes] # Converting to lower case

        if(self.classes[0] == "--help"):
            with open('dataset_downloader/classes.txt', 'r') as fp:
                lines = fp.readlines()
            print("**** Classes ****\n")
            [print(x.split('\n')[0]) for x in lines]
            exit(0)

        print("Classes to download: ", self.classes)
        self.makeDirectory(self.download_directory)
        cats = self.coco.loadCats(self.coco.getCatIds())
        nms=[cat['name'] for cat in cats]
        for name in self.classes:
            if(name not in nms):
                print(f"{name} is not a valid class, Skipping.")
                self.classes.remove(name)

    def get_urls(self):
        """
        Get url to download coco images

        Args:
            None

        Returns:
            urls (string array): COCO urls
            file_names (string array): File names for COCO data
        """
        urls = {}
        file_names = {}

        for i in range(len(self.classes)):
            self.makeDirectory(f'{self.download_directory}/{self.classes[i]}')
            catIds = self.coco.getCatIds(catNms=[self.classes[i]])
            imgIds = self.coco.getImgIds(catIds=catIds)
            images = self.coco.loadImgs(imgIds)
            file_names[self.classes[i]] = [im['file_name'] for im in images]
            urls[self.classes[i]] = [im["coco_url"] for im in images]
        return urls, file_names

    def download(self, url):
        """
        Download coco dataset image with metadata

        Args:
            url (string): Location of file

        Returns:
            None
        """
        try:
            r = requests.get(url)
            file_name = url.split("/")[-1]
            with open(f"{self.download_directory}/{self.curr_class}/{file_name}", "wb") as f:
                f.write(r.content)
            self.img_cnt += 1
            print(f"Downloaded : {file_name}, Class: {self.curr_class} , image count {self.img_cnt}")

        except:
            print("not able to download...skipping...")
            self.except_images.write(url.split("/")[-1] + "\n")

    def download_using_thrds(self, total_urls):
        """
        Map the downloaded data

        Args:
            total_urls (string array): COCO urls

        Returns:
            None
        """
        for className, class_urls in total_urls.items():
            self.curr_class = className
            with ThreadPool(self.MAX_THREADS) as p:
                p.map(self.download, class_urls)

    def write_annotations(self):
        """
        Save COCO annotations into a file

        Args:
            None
        
        Returns:
            None
        """
        for i in range(len(self.classes)):
            catIds = self.coco.getCatIds(catNms=[self.classes[i]])
            imgIds = self.coco.getImgIds(catIds=catIds)
            images = self.coco.loadImgs(imgIds)
            self.write_image_annotations(self.classes[i], images, catIds, imgIds)
    
    def write_image_annotations(self, className, images, catIds, imgIds):
        """
        Save COCO image annotations into a file

        Args:
            className (string): A single class that will be used in training 
            images (object array): Loaded img objects
            catIds (int array): Integer ids specifying cats 
            imgIds (int array): Integer ids specifying img
        
        Returns:
            None
        """
        print(f"Total Images: {len(images)} for class '{className}'")
        
        for im in images:
            image_file_name = im['file_name']
            label_file_name = im['file_name'].split('.')[0] + '.txt'

            fileExists = os.path.exists(f'{self.download_directory}/{className}/{image_file_name}')
            if(fileExists):
                annIds = self.coco.getAnnIds(imgIds=im['id'], catIds=catIds, iscrowd=None)
                anns = self.coco.loadAnns(annIds)    
                s = ""            
                for i in range(len(anns)):
                    xmin = int(anns[i]['bbox'][0])
                    ymin = int(anns[i]['bbox'][1])
                    xmax = int(xmin + anns[i]['bbox'][2])
                    ymax = int(ymin + anns[i]['bbox'][3])
                    s += "0 " + str(xmin) + " " + \
                                str(ymin) + " " + \
                                str(xmax) + " " + \
                                str(ymax) + " "

                    if(i < len(anns) - 1):
                        s += '\n'
                
                with open(f'{self.download_directory}/{className}/{label_file_name}', 'w') as label_handler:
                    label_handler.write(s)
            else:
                print(f"{image_file_name} - Image not downloaded...skipping annotation...")


#downloader = Coco_Dataset_Downloader(["person", "car", "bus", "truck", "bicycle"])
#downloader.start()
