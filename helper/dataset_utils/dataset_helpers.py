"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
dataset_helpers.py:
    Creating a dataset to be used in training
"""
# -*- coding: utf-8 -*-
import numpy as np

from tensorflow.keras.utils import Sequence
from helper.dataset_utils import augment_data
from helper.image_utils.img_helper import read_img, preprocess_img
from helper.ann_utils.ann_helper import read_annotation, decode_class_name


class Create_Dataset(Sequence):
    """
    Create a custom dataset for tensorflow keras to test the training model by using image filters, kernels & algorthings on an exsiting training set
    """
    def __init__(self, mask, anchors, max_outputs, strides, name_path, anno_path, img_size, batch_size, normal_method=True, mosaic=False, groundTruth_smoothing=False, verbose=0):
        """
        Create a Dataset object

        Args:
            mask (int array): Values for mask matrix
            anchors (int array): Values for anchor matrix
            max_outputs (int): Maximum boxes to be drawn in a single image
            strides (int array): Size of the stride layers 
            name_path (string): Path to list of all possible classes
            anno_path (string): Path to annotations file
            img_size (int array): Size of the images used for the model
            batch_size (int): Size of the dataset to be created
            normal_method (bool): Decides if the dataset should use image filters
            mosaic (bool): Decides if the dataset should use mosaic augmentation
            groundTruth_smooth (bool): Decides if to apply a uniform distribution on ground truths
            verbose (int): Change debug level
        
        Returns:
            Create_Dataset (object)
        """
        self.verbose = verbose

        self.mask = mask
        self.anchors = anchors
        self.max_boxes = max_outputs
        self.strides = strides

        self.name_path = name_path
        self.anno_path = anno_path
        self.img_size = img_size
        self.batch_size = batch_size

        self.normal_method = normal_method
        self.mosaic = mosaic
        self.groundTruth_smoothing = groundTruth_smoothing

        self.annotation = read_annotation(anno_path=self.anno_path)
        self.num_anno = len(self.annotation)
        self.name = decode_class_name(self.name_path)
        self.num_classes = len(self.name)

        # init
        self._img_size = np.random.choice(self.img_size)
        self._grid_size = self._img_size // self.strides

    def __len__(self):
        return int(np.ceil(float(len(self.annotation)) / self.batch_size))

    def __getitem__(self, idx):
        """
        Get an image from the dataset and apply image algorthims if configured

        Args:
            idx (int): Index of dataset

        Returns:
            batch_img (cv2.Mat): Image from dataset
            batch_groundTruth (float array): Ground truth of the image
        """
        l_bound = idx * self.batch_size
        r_bound = (idx + 1) * self.batch_size

        if r_bound > len(self.annotation):
            r_bound = len(self.annotation)
            l_bound = r_bound - self.batch_size

        self._on_batch_start(idx)

        batch_img = np.zeros((r_bound - l_bound, self._img_size, self._img_size, 3), dtype=np.float32)
        batch_groundTruth = [np.zeros((r_bound - l_bound, size, size, len(mask_per_layer) * (5 + self.num_classes)),
                                dtype=np.float32)
                       for size, mask_per_layer in zip(self._grid_size, self.mask)]

        for i, sub_idx in enumerate(range(l_bound, r_bound)):
            img, bound_boxes, groundTruths = self._getitem(sub_idx)

            if self.mosaic:
                sub_idx = np.random.choice(np.delete(np.arange(self.num_anno), idx), 3, False)
                img2, bound_boxes2, groundTruths2 = self._getitem(sub_idx[0])
                img3, bound_boxes3, groundTruths3 = self._getitem(sub_idx[1])
                img4, bound_boxes4, groundTruths4 = self._getitem(sub_idx[2])
                img, bound_boxes, groundTruths = augment_data.augment_mosic(img, bound_boxes, groundTruths,
                                                      img2, bound_boxes2, groundTruths2,
                                                      img3, bound_boxes3, groundTruths3,
                                                      img4, bound_boxes4, groundTruths4)
            if self.normal_method:
                img = augment_data.create_random_distort(img)
                img = augment_data.create_random_grayscale(img)
                img, bound_boxes = augment_data.create_random_flip_lr(img, bound_boxes)
                img, bound_boxes = augment_data.create_random_rotate(img, bound_boxes)
                img, bound_boxes, groundTruths = augment_data.create_random_crop_and_zoom(img, bound_boxes, groundTruths,
                                                                     (self._img_size, self._img_size))

            img, bound_boxes, groundTruths = augment_data.filter_bboxes(img, bound_boxes, groundTruths)
            groundTruths = self._preprocess_true_boxes(bound_boxes, groundTruths)

            batch_img[i] = img
            for j in range(len(self.mask)):
                batch_groundTruth[j][i, :, :, :] = groundTruths[j][:, :, :]

        return batch_img, batch_groundTruth

    def _getitem(self, sub_idx):
        """
        Get an image from the dataset

        Args:
            sub_idx (int): Index of databse

        Returns:
            img (cv2.Mat): Image from dataset
            bound_boxes (int array): Bounding boxes for image
            groundTruths (int array): Ground Truth for image
        """
        path, bound_boxes, groundTruths = self.annotation[sub_idx]
        img = read_img(path)

        if len(bound_boxes) != 0:
            bound_boxes, groundTruths = np.array(bound_boxes), np.array(groundTruths)
        else:
            bound_boxes, groundTruths = np.zeros((0, 4)), np.zeros((0,))

        img, bound_boxes = preprocess_img(img, (self._img_size, self._img_size), bound_boxes)
        groundTruths = augment_data.create_onehot(groundTruths, self.num_classes, self.groundTruth_smoothing)

        return img, bound_boxes, groundTruths

    def _preprocess_true_boxes(self, bound_boxes, groundTruths):
        """
        Calculate anchor index for true boxes

        Args:
            bound_boxes (int array): Boxes used to annotate image
            groundTruths (int array) Ground truths of image

        Returns:
            bound_boxes_groundTruth (float array): Bouding boxes with ground truth data
        """
        bound_boxes_groundTruth = [np.zeros((size, size, len(mask_per_layer), 5 + self.num_classes), np.float32)
                        for size, mask_per_layer in zip(self._grid_size, self.mask)]

        bound_boxes = np.array(bound_boxes, dtype=np.float32)
        anchor_area = self.anchors[:, 0] * self.anchors[:, 1]
        bound_boxes_wh = bound_boxes[:, 2:4] - bound_boxes[:, 0:2]

        bound_boxes_wh_exp = np.tile(np.expand_dims(bound_boxes_wh, 1), (1, self.anchors.shape[0], 1))
        boxes_area = bound_boxes_wh_exp[..., 0] * bound_boxes_wh_exp[..., 1]
        intersection = np.minimum(bound_boxes_wh_exp[..., 0], self.anchors[:, 0]) * np.minimum(bound_boxes_wh_exp[..., 1],
                                                                                          self.anchors[:, 1])
        iou = intersection / (boxes_area + anchor_area - intersection + 1e-8)  # (N, A)
        best_anchor_idxs = np.argmax(iou, axis=-1)  # (N,)

        for i, bbox in enumerate(bound_boxes):
            
            search = np.where(self.mask == best_anchor_idxs[i])
            best_detect = search[0][0]
            best_anchor = search[1][0]

            coord_xy = (bbox[0:2] + bbox[2:4]) * 0.5
            coord_xy /= self.strides[best_detect]
            coord_xy = coord_xy.astype(np.int)

            bound_boxes_groundTruth[best_detect][coord_xy[1], coord_xy[0], best_anchor, :4] = bbox
            bound_boxes_groundTruth[best_detect][coord_xy[1], coord_xy[0], best_anchor, 4:5] = 1.
            bound_boxes_groundTruth[best_detect][coord_xy[1], coord_xy[0], best_anchor, 5:] = groundTruths[i, :]

        return [layer.reshape([layer.shape[0], layer.shape[1], -1]) for layer in bound_boxes_groundTruth]

    def _on_batch_start(self, idx, patience=10):
        """
        Randomises vars before getting image. Will print image size if verbose is true

        Args:
            idx (int): Index of dataset
            patience (int): Modulus value to dictate how freqent code is ran

        Returns:
            None
        """
        if idx % patience == 0:
            self._img_size = np.random.choice(self.img_size)
            self._grid_size = self._img_size // self.strides

            if self.verbose:
                print('Change img size to', self._img_size)

    def on_epoch_end(self):
        """
        Shuffle dataset after full iteration of dataset

        Args:
            None
        
        Returns:
            None
        """
        np.random.shuffle(self.annotation)  # shuffle


def decode_class_names(name_path):
    """
    Reads the list of classes to be trained in the model

    Args:
        name_path (string): Path of the classes file
    
    Returns:
        name (string array): Array of what objects the ml model has been trained to detect
    """
    with open(name_path, 'r') as f:
        lines = f.readlines()
    name = []
    for line in lines:
        line = line.strip()
        if line:
            name.append(line)
    return name
