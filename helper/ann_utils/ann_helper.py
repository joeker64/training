"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
ann_helper.py:
    Read and decode data from files
"""

def decode_class_name(classes_path):
    """
    Reads the list of classes to be trained against 

    Args:
        classes_path (string): Path of the classes file
    
    Returns:
        classes (string array): Array of what classes the ml model will be trained for
    """
    with open(classes_path, 'r') as f:
        lines = f.readlines()
    classes = []
    for line in lines:
        line = line.strip()
        if line:
            classes.append(line)
    return classes


def read_annotation(anno_path, type='y_true'):
    """
    Read the annoctaions from the data set coco

    Args:
        anno_path (string): Path to where the file "train.txt" has been created
        type (string): Const set to "y_true" used to chose if it will read "confi" data from the file

    Returns:
        anno (array): Annotation data read from file 
    """
    with open(anno_path, 'r') as f:
        lines = f.readlines()
    anno = []
    for line in lines:
        line = line.strip()
        if line:
            anno.append(read_line(line, type))
    return anno


def read_line(line, type):
    """
    Read lines from file

    Args:
        line (line): Line from .txt file
        type (string): Const set to "y_true" used to chose if it will read "confi" data from the file

    Returns:
        function: Funtion on how to read line
    """
    if type == 'y_true':
        return read_line_y_true(line)
    elif type == 'y_pred':
        return read_line_y_pred(line)
    

def read_line_y_pred(line):
    """
    Read line from annotation data collecting coordinates, label and confi

    Args:
        line (line): Line from the .txt file

    Returns:
        path (string): Path to image that is currently being annotated
        bboxes (int list): coordinates of the box used to annotate image
        labels (string): Label to annotated part of the image
        confi 
    """
    anns = line.split()
    path = anns[0]
    anns = anns[1:]

    bboxes = []
    labels = []
    confis = []
    for ann in anns:
        if not ann:
            continue
        x1, y1, x2, y2, label, confi = ann.split(',')
        x1, y1, x2, y2, label, confi = float(x1), float(y1), float(x2), float(y2), int(label), float(confi)
        bboxes.append([x1, y1, x2, y2])
        labels.append(label)
        confis.append(confi)

    return path, bboxes, labels, confis


def read_line_y_true(line):
    """
    Read line from annotation data collecting coordinates and label  

    Args:
        line (line): Line from the .txt file

    Returns:
        path (string): Path to image that is currently being annotated
        bboxes (int list): coordinates of the box used to annotate image
        labels (string): Label to annotated part of the image
    """
    anns = line.split()
    path = anns[0]
    anns = anns[1:]

    bboxes = []
    labels = []
    for ann in anns:
        if not ann:
            continue
        x1, y1, x2, y2, label = ann.split(',')
        x1, y1, x2, y2, label = float(x1), float(y1), float(x2), float(y2), float(label)
        bboxes.append([x1, y1, x2, y2])
        labels.append(label)

    return path, bboxes, labels
