"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
iou_losses.py:
    Calculate Intersection over Union (IoU) with different algorithms 
"""

# -*- coding: utf-8 -*-
import tensorflow as tf
import math
epsilon = 1e-8


def compute_GIoU_loss(y_out_bbox, y_true_bbox):
    """
    Calculate Generalized Intersection over Union (GIoU)

    Args:
        y_out_bbox  (int array): Model generated bounding box
        y_true_bbox (int array): Grounded truth bounding box

    Returns:
        giou_loss (float):  Generalized Intersection over Union score 
    """
    y_out_bbox_min = y_out_bbox[..., :2]
    y_out_bbox_max = y_out_bbox[..., 2:4]
    y_out_bbox_wh = y_out_bbox_max - y_out_bbox_min

    y_true_bbox_min = y_true_bbox[..., :2]
    y_true_bbox_max = y_true_bbox[..., 2:4]
    y_true_bbox_wh = y_true_bbox_max - y_true_bbox_min

    iou_min = tf.maximum(y_true_bbox_min, y_out_bbox_min)
    iou_max = tf.minimum(y_true_bbox_max, y_out_bbox_max)
    iou_wh = tf.maximum(iou_max - iou_min, 0.)
    iou_area = iou_wh[..., 0] * iou_wh[..., 1]
    y_out_bbox_area = y_out_bbox_wh[..., 0] * y_out_bbox_wh[..., 1]
    y_true_bbox_area = y_true_bbox_wh[..., 0] * y_true_bbox_wh[..., 1]
    union_area = y_out_bbox_area + y_true_bbox_area - iou_area
    # calculate IoU, add epsilon in denominator to avoid dividing by 0
    computed_iou = iou_area / tf.maximum(union_area, epsilon)

    # get enclosed area
    enclose_min = tf.minimum(y_true_bbox_min, y_out_bbox_min)
    enclose_max = tf.maximum(y_true_bbox_max, y_out_bbox_max)
    enclose_wh = tf.maximum(enclose_max - enclose_min, 0.0)
    enclose_area = enclose_wh[..., 0] * enclose_wh[..., 1]
    # calculate compute_GIoU_loss, add epsilon in denominator to avoid dividing by 0
    giou_loss = computed_iou - (enclose_area - union_area) / tf.maximum(enclose_area, epsilon)

    return giou_loss


def compute_DIoU_loss(y_out_bbox, y_true_bbox):
    """
    Calculate distance intersection over union (DIoU)

    Args:
        y_out_bbox  (int array): Model generated bounding box
        y_true_bbox (int array): Grounded truth bounding box

    Returns:
        diou_loss (float): Distance intersection over union score
    """
    y_out_bbox_min = y_out_bbox[..., :2]
    y_out_bbox_max = y_out_bbox[..., 2:4]
    y_out_bbox_wh = y_out_bbox_max - y_out_bbox_min
    y_out_bbox_center = (y_out_bbox_min + y_out_bbox_max) / 2.

    y_true_bbox_min = y_true_bbox[..., :2]
    y_true_bbox_max = y_true_bbox[..., 2:4]
    y_true_bbox_wh = y_true_bbox_max - y_true_bbox_min
    y_true_bbox_center = (y_true_bbox_min + y_true_bbox_max) / 2.

    iou_min = tf.maximum(y_out_bbox_min, y_true_bbox_min)
    iou_max = tf.minimum(y_out_bbox_max, y_true_bbox_max)
    iou_wh = tf.maximum(iou_max - iou_min, 0.)
    iou_area = iou_wh[..., 0] * iou_wh[..., 1]
    y_true_bbox_area = y_true_bbox_wh[..., 0] * y_true_bbox_wh[..., 1]
    y_out_bbox_area = y_out_bbox_wh[..., 0] * y_out_bbox_wh[..., 1]
    union_area = y_true_bbox_area + y_out_bbox_area - iou_area
    # calculate IoU, add epsilon in denominator to avoid dividing by 0
    computed_iou = iou_area / tf.maximum(union_area, epsilon)

    # bbox center distance
    center_distance = tf.reduce_sum(tf.square(y_out_bbox_center - y_true_bbox_center), axis=-1)
    # get enclosed area
    enclose_min = tf.minimum(y_out_bbox_min, y_true_bbox_min)
    enclose_max = tf.maximum(y_out_bbox_max, y_true_bbox_max)
    enclose_wh = tf.maximum(enclose_max - enclose_min, 0.0)
    # get enclosed diagonal distance
    enclose_diagonal = tf.reduce_sum(tf.square(enclose_wh), axis=-1)
    # calculate compute_, ad_lossd epsilon in denominator to avoid dividing by 0
    diou_loss = computed_iou - center_distance / tf.maximum(enclose_diagonal, epsilon)

    return diou_loss


def compute_CIoU_loss(y_out_bbox, y_true_bbox):
    """
    Calculate complete intersection over union (CIoU)

    Args:
        y_out_bbox  (int array): Model generated bounding box
        y_true_bbox (int array): Grounded truth bounding box

    Returns:
        ciou_loss (float): Complete intersection over union score
    """
    y_out_bbox_min = y_out_bbox[..., :2]
    y_out_bbox_max = y_out_bbox[..., 2:4]
    y_out_bbox_wh = y_out_bbox_max - y_out_bbox_min
    y_out_bbox_center = (y_out_bbox_min + y_out_bbox_max) / 2.

    y_true_bbox_min = y_true_bbox[..., :2]
    y_true_bbox_max = y_true_bbox[..., 2:4]
    y_true_bbox_wh = y_true_bbox_max - y_true_bbox_min
    y_true_bbox_center = (y_true_bbox_min + y_true_bbox_max) / 2.

    iou_min = tf.maximum(y_out_bbox_min, y_true_bbox_min)
    iou_max = tf.minimum(y_out_bbox_max, y_true_bbox_max)
    iou_wh = tf.maximum(iou_max - iou_min, 0.)
    iou_area = iou_wh[..., 0] * iou_wh[..., 1]
    y_true_bbox_area = y_true_bbox_wh[..., 0] * y_true_bbox_wh[..., 1]
    y_out_bbox_area = y_out_bbox_wh[..., 0] * y_out_bbox_wh[..., 1]
    union_area = y_true_bbox_area + y_out_bbox_area - iou_area
    # calculate IoU, add epsilon in denominator to avoid dividing by 0
    computed_iou = iou_area / tf.maximum(union_area, epsilon)

    # bbox center distance
    center_distance = tf.reduce_sum(tf.square(y_out_bbox_center - y_true_bbox_center), axis=-1)
    # get enclosed area
    enclose_min = tf.minimum(y_out_bbox_min, y_true_bbox_min)
    enclose_max = tf.maximum(y_out_bbox_max, y_true_bbox_max)
    enclose_wh = tf.maximum(enclose_max - enclose_min, 0.0)
    # get enclosed diagonal distance
    enclose_diagonal = tf.reduce_sum(tf.square(enclose_wh), axis=-1)
    # calculate compute_, ad_lossd epsilon in denominator to avoid dividing by 0
    diou_loss = computed_iou - center_distance / tf.maximum(enclose_diagonal, epsilon)

    # calculate param v and alpha to extend to CIoU
    constant = 4. / (math.pi * math.pi)
    v = constant * tf.square(
        tf.math.atan2(y_true_bbox_wh[..., 0], tf.maximum(y_true_bbox_wh[..., 1], epsilon)) - tf.math.atan2(
            y_out_bbox_wh[..., 0], tf.maximum(y_out_bbox_wh[..., 1], epsilon)))
    alpha = v / tf.maximum(1.0 - computed_iou + v, epsilon)
    ciou_loss = diou_loss - alpha * v
    return ciou_loss
