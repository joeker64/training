"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
tiny_model.py:
    Build a tiny_yolov3 model
"""

import tensorflow as tf

from helper.losses_utils.iou_losses import compute_GIoU_loss, compute_DIoU_loss, compute_CIoU_loss

WEIGHT_DECAY = 0.  # 5e-4
LEAKY_ALPHA = 0.1


def myModelConv2D(*args, **kwargs):
    """
    Implement a 2D convolution layer for tiny_yolov3 model

    Args:
        *args: Configurtion for Conv2D layer
        **kwargs: Configurtion for Conv2D layer
    
    Returns:
        tf.keras.layers.Conv2D: 2D convolution layer
    """
    myModel_conv_kwargs = {"kernel_regularizer": tf.keras.regularizers.l2(WEIGHT_DECAY),
                           "kernel_initializer": tf.keras.initializers.RandomNormal(mean=0.0, stddev=0.01),
                           "padding": "valid" if kwargs.get(
                               "strides") == (2, 2) else "same"}
    myModel_conv_kwargs.update(kwargs)

    return tf.keras.layers.Conv2D(*args, **myModel_conv_kwargs)


def myModelConv2D_BN_Leaky(*args, **kwargs):
    """
    Implement a 2D convolution layer & batch normalization leaky ReLu for tiny_yolov3 model

    Args:
        *args: Configurtion for layer
        **kwargs: Configurtion for layer
    
    Returns:
        wrapper (function): Function to create a 2D convolution layer & batch normalization leaky ReLu Layer
    """
    without_bias_kwargs = {"use_bias": False}
    without_bias_kwargs.update(kwargs)

    def wrapper(x):
        """
        Create a 2D convolution & batch normalization leaky ReLu layer

        Args:
            x (tf.keras.layers.Layer): Keras model layer

        Returns:
            x (tf.keras.layers.Layer): 2D convolution layer & batch normalization leaky ReLu Layer
        """
        x = myModelConv2D(*args, **without_bias_kwargs)(x)
        x = tf.keras.layers.BatchNormalization()(x)
        x = tf.keras.layers.LeakyReLU(alpha=LEAKY_ALPHA)(x)
        return x

    return wrapper



def my_model(iou_threshold, score_threshold, max_outputs, num_classes, strides, mask, anchors,
                input_size=None,
                name=None):
    """
    Create the tiny_yolov3 model

    Args:
        iou_threshold (float): Threshold for the Intersection over Union (IoU) algorthim 
        score_threshold (float): Minimum value class score for it to be valid
        max_outputs (int): Max objects to be detected in a single frame
        num_classes (int): Total number of classes the model can detect
        strides (int array): Configures number and size of stride layers for downsampling the image
        mask (int array): Matrix used for ML masking
        anchors (int array): Marix containing predefined bounding boxes known as anchor boxes
        input_size (None): Size of input image
        name (None): Name of model. Currently set to None

    Returns:
        model (tf.keras.Model): A tensorflow keras model with layers configured to tiny_yolov3
    """
    if input_size is None:
        x = inputs = tf.keras.Input([None, None, 3])
    else:
        x = inputs = tf.keras.Input([input_size, input_size, 3])

    # -----
    x = myPreprocessInput()(x)
    x = myModelConv2D_BN_Leaky(16, 3)(x)
    x = tf.keras.layers.MaxPool2D(2, 2, "same")(x)
    x = myModelConv2D_BN_Leaky(32, 3)(x)
    x = tf.keras.layers.MaxPool2D(2, 2, "same")(x)
    x = myModelConv2D_BN_Leaky(64, 3)(x)
    x = tf.keras.layers.MaxPool2D(2, 2, "same")(x)
    x = myModelConv2D_BN_Leaky(128, 3)(x)
    x = tf.keras.layers.MaxPool2D(2, 2, "same")(x)
    x = x_8 = myModelConv2D_BN_Leaky(256, 3)(x)
    x = tf.keras.layers.MaxPool2D(2, 2, "same")(x)
    x = myModelConv2D_BN_Leaky(512, 3)(x)
    x = tf.keras.layers.MaxPool2D(2, 1, "same")(x)
    x = myModelConv2D_BN_Leaky(1024, 3)(x)

    x = x_13 = myModelConv2D_BN_Leaky(256, 1)(x)
    x = myModelConv2D_BN_Leaky(512, 3)(x)
    output_0 = myModelConv2D(len(mask[0]) * (num_classes + 5), 1)(x)

    x = myModelConv2D_BN_Leaky(128, 1)(x_13)
    x = tf.keras.layers.UpSampling2D(2)(x)
    x = tf.keras.layers.Concatenate()([x, x_8])

    x = myModelConv2D_BN_Leaky(256, 3)(x)
    output_1 = myModelConv2D(len(mask[1]) * (num_classes + 5), 1)(x)

    model = tf.keras.Model(inputs, (output_0, output_1), name=name)


    return model



def _broadcast_iou(box_1, box_2):
    """
    Calculate Intersection over Union (IoU) with two boxes  

    Args:
        box_1 (tf.Tensor): Box to calculate IoU
        box_2 (tf.Tensor): Box to calculate IoU
    
    Returns:
        (float): IoU value 
    """
    # box_1: (..., (x1, y1, x2, y2))
    # box_2: (N, (x1, y1, x2, y2))

    # broadcast boxes
    box_1 = tf.expand_dims(box_1, -2)
    box_2 = tf.expand_dims(box_2, 0)
    # new_shape: (..., N, (x1, y1, x2, y2))
    new_shape = tf.broadcast_dynamic_shape(tf.shape(box_1), tf.shape(box_2))
    box_1 = tf.broadcast_to(box_1, new_shape)
    box_2 = tf.broadcast_to(box_2, new_shape)

    int_w = tf.maximum(tf.minimum(box_1[..., 2], box_2[..., 2]) - tf.maximum(box_1[..., 0], box_2[..., 0]), 0)
    int_h = tf.maximum(tf.minimum(box_1[..., 3], box_2[..., 3]) - tf.maximum(box_1[..., 1], box_2[..., 1]), 0)
    int_area = int_w * int_h
    box_1_area = (box_1[..., 2] - box_1[..., 0]) * (box_1[..., 3] - box_1[..., 1])
    box_2_area = (box_2[..., 2] - box_2[..., 0]) * (box_2[..., 3] - box_2[..., 1])
    return int_area / tf.maximum(box_1_area + box_2_area - int_area, 1e-8)


def modelLoss(anchors, stride, num_classes, ignore_thresh, type):
    """
    Returns function to calculate the loss of accuracy of the model on a prediction 

    Args:
        anchors (int array): Marix containing predefined bounding boxes known as anchor boxes
        stride (int array): Configures number and size of stride layers for downsampling the image
        num_classes (int): Total number of classes the model can detect
        ignore_thresh (float): Threshold to ignore false positives
        type (string): Select which IoU algorthim to use

    Returns:
        wrapper (function): Function that calcualtes the model loss
    """
    def wrapper(y_true, y_pred):
        """
        Loss function used with Keras to calcuate accuracy of model

        Args:
            y_true (int array): Ground truth values the shape being '[batch_size, d0, .. dN]'
            y_pred (int array): The predicted values theshape being '[batch_size, d0, .. dN]'

        Returns:
            (float): Loss value of the model
        """
        # 0. default
        dtype = y_pred.dtype
        y_shape = tf.shape(y_pred)
        grid_w, grid_h = y_shape[2], y_shape[1]
        anchors_tensor = tf.cast(anchors, dtype)
        y_true = tf.reshape(y_true, (y_shape[0], y_shape[1], y_shape[2], anchors_tensor.shape[0], num_classes + 5))
        y_pred = tf.reshape(y_pred, (y_shape[0], y_shape[1], y_shape[2], anchors_tensor.shape[0], num_classes + 5))

        # 1. transform all pred outputs
        # y_pred: (batch_size, grid, grid, anchors, (x, y, w, h, obj, ...cls))
        pred_xy, pred_wh, pred_obj, pred_cls = tf.split(y_pred, (2, 2, 1, num_classes), axis=-1)

        # !!! grid[x][y] == (y, x)
        grid = tf.meshgrid(tf.range(grid_w), tf.range(grid_h))
        grid = tf.expand_dims(tf.stack(grid, axis=-1), axis=2)  # [gx, gy, 1, 2]

        pred_xy = (tf.sigmoid(pred_xy) + tf.cast(grid, dtype)) * stride
        pred_wh = tf.exp(pred_wh) * anchors_tensor
        pred_obj = tf.sigmoid(pred_obj)
        pred_cls = tf.sigmoid(pred_cls)

        pred_wh_half = pred_wh / 2.
        pred_x1y1 = pred_xy - pred_wh_half
        pred_x2y2 = pred_xy + pred_wh_half
        pred_box = tf.concat([pred_x1y1, pred_x2y2], axis=-1)

        # 2. transform all true outputs
        # y_true: (batch_size, grid, grid, anchors, (x1, y1, x2, y2, obj, cls))
        true_box, true_obj, true_cls = tf.split(y_true, (4, 1, num_classes), axis=-1)
        true_xy = (true_box[..., 0:2] + true_box[..., 2:4]) / 2.
        true_wh = true_box[..., 2:4] - true_box[..., 0:2]

        # give higher weights to small boxes
        box_loss_scale = 2 - true_wh[..., 0] * true_wh[..., 1] / (
            tf.cast(tf.reduce_prod([grid_w, grid_h, stride, stride]), dtype))

        # 4. calculate all masks
        obj_mask = tf.squeeze(true_obj, -1)
        # ignore false positive when iou is over threshold
        best_iou = tf.map_fn(
            lambda x: tf.reduce_max(_broadcast_iou(x[0], tf.boolean_mask(x[1], tf.cast(x[2], tf.bool))), axis=-1),
            (pred_box, true_box, obj_mask),
            dtype)
        ignore_mask = tf.cast(best_iou < ignore_thresh, dtype)

        # 5. calculate all losses
        if 'L2' in type:
            xy_loss = 0.5 * tf.reduce_sum(tf.square(true_xy - pred_xy), axis=-1)
            wh_loss = 0.5 * tf.reduce_sum(tf.square(true_wh - pred_wh), axis=-1)
            box_loss = xy_loss + wh_loss
        elif 'GIoU' in type:
            giou = compute_GIoU_loss(pred_box, true_box)
            box_loss = 1. - giou
        elif 'DIoU' in type:
            diou = compute_DIoU_loss(pred_box, true_box)
            box_loss = 1. - diou
        elif 'CIoU' in type:
            ciou = compute_CIoU_loss(pred_box, true_box)
            box_loss = 1. - ciou
        else:
            raise NotImplementedError('Loss Type', type, 'is Not Implemented!')

        box_loss = obj_mask * box_loss_scale * box_loss
        obj_loss = tf.keras.losses.binary_crossentropy(true_obj, pred_obj)
        obj_loss = obj_mask * obj_loss + (1 - obj_mask) * ignore_mask * obj_loss
        cls_loss = obj_mask * tf.keras.losses.binary_crossentropy(true_cls, pred_cls)

        def _focal_loss(y_true, y_pred, alpha=1, gamma=2):
            focal_loss = tf.squeeze(alpha * tf.pow(tf.abs(y_true - y_pred), gamma), axis=-1)
            return focal_loss

        if 'FL' in type:
            focal_loss = _focal_loss(true_obj, pred_obj)
            obj_loss = focal_loss * obj_loss

        # 6. sum over (batch, gridx, gridy, anchors) => (batch, 1)
        box_loss = tf.reduce_mean(tf.reduce_sum(box_loss, axis=(1, 2, 3)))
        obj_loss = tf.reduce_mean(tf.reduce_sum(obj_loss, axis=(1, 2, 3)))
        cls_loss = tf.reduce_mean(tf.reduce_sum(cls_loss, axis=(1, 2, 3)))

        return box_loss + obj_loss + cls_loss

    return wrapper

class myPreprocessInput(tf.keras.layers.Layer):
    """
    Child class of keras layer to apply preprocess on layer objects
    """
    def __init__(self, **kwargs):
        super(myPreprocessInput, self).__init__(**kwargs)

    def build(self, input_shape):
        super(myPreprocessInput, self).build(input_shape)  # Be sure to call this at the end

    def call(self, inputs, **kwargs):
        x = tf.divide(inputs, 255.)
        return x

    def get_config(self):
        config = super(myPreprocessInput, self).get_config()
        return config

    def compute_output_shape(self, input_shape):
        return input_shape


