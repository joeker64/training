# Setting Model training on Alibaba Cloud
## Introduction
This document provides step by step setup of model training on Alibaba Cloud. The
trained model will be optimized through Tensorflow -> tflite -> ONNX -> Optimized
ONNX format and the trained model will be stored in the OSS bucket of Alibaba cloud.

## Pre-Requisites
- Alibaba Cloud Service Account.
- OSS, Machine Learning Platform for AI, Container Registry services must be
activated for the Alibaba Cloud account.
- x86 host machine with docker installed.

## Setup Guide
Clone the project
Run the command given below to clone the project.
```sh
$ git clone -b <BRANCH_NAME> <PROJECT_GIT_URL>
```

Directory Structure for cloned project
- <PROJECT_ROOT>
    - config/
    - docs/
    - dataset_downloader/
    - helper/
    - model_data/
    - Dockerfile_AWS
    - Dockerfile_Alibaba
    - Dockerfile_Local_training
    - train_model.py
    - dataset_downloader.py
    - README_Dataset_downloading.md
    - README_AliCloud_Model_Training_setup.md
    - README_AWS_Model_Training_setup.md
    - README_Local_Model_Training_setup.md


## Configuring Resource Access Management user in Alibaba Cloud
A Resource Access Management (RAM) user is an entity created on Alibaba Cloud to
allow end user to interact with Alibaba Cloud Services and authorize different access
and permissions. Please Follow the below steps to setup the RAM.
- Open the Alibaba Cloud Page & Login with the Account Created.
- Search for ‘Resource Access Management’ on Alibaba Cloud console.
- Click on ‘Resource Access Management’ from the search result, it will open the
Resource Access Management console.
- On the left-side navigation pane, choose “Identities > Users”.
- On the Users page, click on “Create User”.
- In the User Account Information section of the Create User page,
    - Enter the New Logon Name and Display Name parameters.
    - In the Access Mode section, select both of the access modes mentioned below.
        - Console Access
            - Select this option and complete the logon security settings.
            - Select system-generated password.
            - Select password reset not required
            - Select multi-factor authentication (MFA) not required.
        - OpenAPI Access
            - Upon selection, an AccessKey pair is automatically created for the RAM user.
            - The RAM user can call API operations or use other development tools to access Alibaba Cloud resources.
- After Configuring the Create User form, Click on OK.
- Download CSV file which consists of accesskey and secretkey.
- Click on the “Return” button.
- In the Actions column of the newly created user, click on “Add Permissions”.
- Under “Select Policies”, Click on “Enter policy name” box select 
    - AliyunOSSFullAccess
    - AliyunContainerRegistryFullAccess
    - AliyunPAIEASFullAccess
    - AliyunLogFullAccess
- Click on OK and then Complete button.

## Creating the OSS bucket
- Object Storage Service (OSS) stores data as objects within buckets.
- Search for ‘Object Storage Service’ on Alibaba Cloud console.
- Click on the ‘Object Storage Service’ from search result, it will open Object Storage Service console.
- In the left-side navigation pane, click on Buckets and click Create Bucket on bucket home page.
- In the Create Bucket panel, configure the required parameters like the name of bucket, and the region.
- Select preferred region for OSS bucket. In our case, we have used China (Shanghai) region.
- Let the remaining configuration in the configuration form to its default values.
- This bucket is getting prepared for storing the compiled model by TVM.
- Click on OK.

## Build the docker image
- Navigate to the cloned project folder on machine, open the config file alibaba_config.json located in Model_training/config/ folder in any program
editor.
- Update access_key, secret_key and region id (region is region code for which
region you are going to use service preffered China, Shanghai region for other regions please refer to README.md located at root of this project. project).
The access_key and secret_key will be present in .csv file which were downloaded during creation of user.
- Update the OSS bucket name created for saving trained model.
- Save the config file.
- Open a new terminal on the local machine and navigate to the root of the cloned project folder.
- Change the working directory to root of project folder
```sh
$ cd <PROJECT_ROOT>/
```
- Execute the below command to build the docker image.
  - The user needs to have good internet connection as the image will download the dataset.
    ```sh
    $ sudo docker build -t <IMAGE_NAME>:<IMAGE_TAG> \
    -f Dockerfile_Alibaba .
    ```
    - IMAGE_NAME, is name of the docker image.
    - TAG_NAME, is to tag the different variant of same image.
    - Users can specify the IMAGE_NAME & TAG_NAME of their choice.

## Creating a repository in Alibaba Container Registry
- Go to Alibaba Cloud console and search for container registry in the top
search bar.
- Click on Container registry from search results.
- Select the preferred region (recommended shanghai region for model training) at the top navigation bar.
- Click on Instances in the left side menu and click on Instance of Personal Edition.
- Click on Repository in the left side menu.
- Click on Namespace in the left side menu and click on Create namespace.
- Provide a name for the namespace of user choice and click on complete.
- Click on Repositories on the left side menu and click on Create Repository
- Select the namespace created previously from dropdown list and provide a Repository name keep Repository type as Private and provide Summary for user reference click on Next.
- Select Local repository on top and click on create repository.

## Pushing the docker image to Container Registry
- After creating the repository, the Alibaba cloud will provide you with the steps for pushing an image.
- Open a terminal on the local device.
- Update the below command with user name as the email which was used to
create Alibaba account and execute in local terminal. Use the password of the Alibaba cloud for password to login.
```sh
$ sudo docker login --username=<EMAIL_ID> registry-intl.cn-shanghai.aliyuncs.com
```
- After the login succeeds, Update the below command and paste it in local terminal and change the image name and tag.
    ```sh
    $ sudo docker tag <IMAGE_NAME>:<IMAGE_TAG> registry-intl.<REGION>.aliyuncs.com/<NAMESPACE>/<REPOSITORY_NAME>:<NEW_IMAGE_TAG>
    ```
    - <REGION>: Region id of the preferred region.
    - <IMAGE_NAME >:<IMAGE_TAG>: Image name and tag which were provided during building of docker image for training.
    - <NAMESPACE>: Namespace which was created previously.
    - <REPOSITORY_NAME>: Name of the repository which was created
    previously.
    - <NEW_IMAGE_TAG>: Provide a new image tag of user choice.
- Update the below command and execute on local terminal to push the image
to container registry.
```sh
$ sudo docker push registry-intl.<REGION>.aliyuncs.com/<NAMESPACE>/<REPOSITORY_NAME>:<NEW_IMAGE_TAG>
```
- For more information on pushing and pulling images from container registry,
navigate to the repositories page in container registry and click on repository
created previously and guide will be provided by Alibaba Cloud.
- After the pushing is done, in the Container Registry guide on the left side menu
select tags.
- We can see the pushed image with the tag provided.

## Adding the image to Machine learning platform for AI on Alibaba cloud

- Open the Alibaba Cloud console and search for Machine learning Platform for
AI.
- If the workspace is not selected select the default workspace created
during activation of Machine learning platform for AI service. If selected
we can see the selected workspace on the top of the left side menu in
the dropdown.
- On the left side menu, under AI computing Asset Management, select
images.
- Select custom images, and click on Add image.
- Provide a custom image name.
- In the Associate image with container registry provide the name in below
format.
registry-intl.<REGION>.aliyuncs.com/<NAME_SPACE>/<REPO_NAME>:<IMAGE_TAG>
    - <REGION>: Region id in which the image has been pushed to container registry
    - <NAME_SPACE>: Namespace created during creation of repository.
    - <REPO_NAME>: Repository name which was created previously.
    - <IMAGE_TAG>: Image tag given during pushing of the image.
- Under CPU/GPU select GPU and click on add image.

## Creating and submitting a job
- Go to Machine learning Platform for AI console.
- On the left side menu select jobs.
- Click on create job.
- Provide the Job name, Node image as customized and in the dropdown select the custom image which was created previously.
- For the execution command provide the following command.
    ```sh
    $ python3 train_model.py -m <MODEL_TYPE> -c <CLOUD_PLATFOEM> \
    -e <NUM_EPOCHS>
    ```
    Args:
    - -m: Type of model user wants to train supported models are yolov3/tiny_yolov3
    - -c: Cloud platform that user is training the model on. Supportedplatforms are AWS/Alibaba.
    - -e: Number of epochs the model should be trained. The preferred value would be 1500.
    - In the resources tab select the GPU and select the instance in which the model is to be trained. The minimum configuration of the gpu instance is 4 CPU, 30G disk space 1*Nvidia P100 (ecs.gn5-c4g1.xlarge).
    - Keep all the other options as default.
    - Submit the job
    - The job created may take a minimum of 30 minutes to start the execution of training script.

## View the training logs of the trained model
- Go to Machine learning Platform for AI console and on left side menu select
jobs.
- Select the job which was created and after the job has started, we can see the
logs on the bottom side by selecting the logs option.
- After the model training has completed, we can see the trained model saved on
OSS and under the bucket name created by the user.

## What's next, 
- Refer to Model compilation repository. 
  - ARM-Smart_camera_CSP_Alibaba_TVM_Compilation.pdf 

 