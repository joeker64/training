"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
dataset_downloader.py:
    Start downloading the COCO dataset and merge the data
"""
from dataset_downloader.coco_image_downloader import Coco_Dataset_Downloader
from dataset_downloader.merge_coco_annotations import Merge_COCO_annotations
from dataset_downloader.annotation import annotate
from datetime import datetime

now = datetime.now()
current_time = now.strftime("%H:%M:%S")
print("Current Time =", current_time)

classes = ["person", "car", "bus", "truck", "bicycle"]

downloader = Coco_Dataset_Downloader(classes)
downloader.start()
merger = Merge_COCO_annotations()
merger.merge()
annotate()


now = datetime.now()
current_time = now.strftime("%H:%M:%S")
print("Current Time =", current_time)
