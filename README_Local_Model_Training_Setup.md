
# Setting Model Training on Local Machine
## Introduction
This document provides step by step setup of model training on Local host. The 
trained model will be saved inside the container and will provide us with link to 
download the final trained model.

## Pre-Requisites
 - NVidia Graphics Enabled & Cuda installed Host Machine.
 - Docker installed inside the host system.

## Setup Guide
### Clone the project
- Run the command given below to clone the project.
```sh
$ git clone -b <BRANCH_NAME> <PROJECT_GIT_URL>
```
### Directory Structure for cloned project
- <PROJECT_ROOT>
  - config/
  - docs/
  - dataset_downloader/
  - helper/
  - model_data/
  - Dockerfile_AWS
  - Dockerfile_Alibaba
  - Dockerfile_Local_training
  - train_model.py
  - dataset_downloader.py
  - README_Dataset_downloading.md
  - README_AliCloud_Model_Training_Setup.md
  - README_AWS_Model_Training_Setup.md
  - README_Local_Model_Training_Setup.md

## Build the docker image
- Navigate to the cloned project folder on host machine. 
- Open a new terminal on the local machine and navigate to the root of the cloned project folder.
- Change the working directory to root of project folder
```sh
$ cd <PROJECT_ROOT>/
```
- Execute the below command to build the docker image
      - The user needs to have good internet connection as the image will download the dataset.
```sh
$ sudo docker build -t <IMAGE_NAME>:<IMAGE_TAG> -f Dockerfile_Local_training .
```

  - IMAGE_NAME, is name of the docker image.
  - TAG_NAME, is to tag the different variant of same image.
  - Users can specify the IMAGE_NAME & TAG_NAME of their choice

## Run the docker container
- Use the below command to run the trained model and start the training.
```sh
## Assuming your host machine is already Enabled with NVidia GPU support.

$ docker run –network=host --gpus all --name model_training <IMAGE_NAME>:<IMAGE_TAG> -m <MODEL_TYPE> -e <NUM_EPOCHS> --local
```
- Args: 
  - -m: Type of model user wants to train supported models are `yolov3/tiny_yolov3`
  - -e: Number of epochs the model should be trained. The preferred value would be 1500.
  - --local: When given the model will be trained without any cloud configurations and saves the model locally and later provides the flask link to download the model.
    - Note: The download link will be in format of `https://<IP_ADDRESS_OF_HOST>:8050/download`
  - -c: Cloud platform that user is training the model on. Supported platforms are AWS/Alibaba. This argument is not given because the model is training locally

- Sample command:
```sh
$ docker run --network=host --gpus all --name model_training train_model:local -m tiny_yolov3 -e 1500 --local
```

- If the model is not downloading through the flask URL use the below command to copy the file from docker container environment to local host.
```sh
$ docker cp model_training:/trained_model.h5 .
```
## What's next, 
- Refer to Model compilation repository. 
  - ARM-Smart_camera_CSP_Alibaba_TVM_Compilation.pdf 
