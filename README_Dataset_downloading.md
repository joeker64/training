## Dataset downloader module.
### prerequisites
- A Linux host maching with high end configuration.
- minimum 50GB space available on root partition.

The dataset will be downloaded when the docker starts building and it will take approximately 2 to 3 hours(Tested with Intel Xeon(R) CPU and 2080Ti GPU).

The module will be running 24 simultaneous threads to download the images and for extracting the annotation and preparing the final dataset. Please make sure the host system specifications can handle 24 threads.

If in any case the dataset downloader is taking longer than expected. Please follow the below steps to download it and copy the dataset during Docker container building.
- Navigate to root of project folder
- Execute the below commands to download the dataset.
```sh
$ mkdir dataset/
$ mkdir dataset/images
$ python3 dataset_downloader.py
$ rm downloaded_images -r
```
- After the dataset has downloaded replace the following commands in respective Dockerfile with the below command
- Replace the below lines 
```sh
RUN mkdir dataset/
RUN mkdir dataset/images
COPY dataset_downloader/ dataset_downloader/
COPY dataset_downloader.py dataset_downloader.py
RUN python3 dataset_downloader.py
RUN rm downloaded_images -r
```
- with
```sh
COPY dataset/ dataset/
```
- Build the docker container.